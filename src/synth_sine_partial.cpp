/*
   AudioSynthSinePartial

   AudioSynthSinePartial is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

   The mathematics behind the SyntSinePartialFast class is decribed in
   https://lpsa.swarthmore.edu/Fourier/Series/ExFS.html

MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "synth_sine_partial.h"

/***********************************************************************
 * AudioSynthSinePartial Base class
 ***********************************************************************/
#if defined(USE_TEENSY)
AudioSynthSinePartial::AudioSynthSinePartial(uint8_t p=MIN_PARTIALS+1) : AudioStream(0, NULL)
#else
AudioSynthSinePartial::AudioSynthSinePartial(uint8_t p=MIN_PARTIALS+1)
#endif
{
        _partials=p;
        _max_partials=p+1;
        _frequency=0.0;
        _amplitude=0.0;
	_phase_increment=0.0;
	_phase_accumulator=0.0;
	_bypass=true;
	_resolution=PERIOD/float(DEFAULT_RESOLUTION);
	_rising=false;
	_a = new float[_max_partials]();
	_b = new float[_max_partials]();
	_waveform=-1;
        _yn=0.0;
        _R=0.0;
}

AudioSynthSinePartial::~AudioSynthSinePartial()
{
        delete[] _a;
        delete[] _b;
}

void AudioSynthSinePartial::begin(void)
{
	if(_partials<MIN_PARTIALS+1)
		_max_partials=MIN_PARTIALS+2;
	else if(_partials>MAX_PARTIALS-1)
		_max_partials=MAX_PARTIALS;

	if(uint8_t(_partials + 0.5) > _max_partials-1)
		_partials=(_max_partials-1);

	if(get_resolution() < (_max_partials -1) * 2)
		set_resolution((_max_partials -1) * 2);
	else
		_calculate();

	bypass(false);
}

void AudioSynthSinePartial::frequency(float freq)
{
        if(freq < 0.0)
                _frequency=0.0;
        if(freq > (AUDIO_SAMPLE_RATE_EXACT/2.0))
                _frequency=(AUDIO_SAMPLE_RATE_EXACT/2.0);
        else
                _frequency=freq;

        _phase_increment=(TWO_PI*_frequency)/AUDIO_SAMPLE_RATE_EXACT;
        _phase_accumulator=0.0;
}

void AudioSynthSinePartial::amplitude(float n)
{
        if(n <= 0.0)
	{
                _amplitude=0.0;
		_phase_accumulator=0.0;
	}
        else
	{
		if(_waveform >= 0)
			_amplitude=damping_factor[_waveform] * n;
		else
                	_amplitude=DAMPING_FACTOR * n;
	}
}

uint8_t AudioSynthSinePartial::get_max_partials(void)
{
	return(_max_partials-1);
}

void AudioSynthSinePartial::set_partials(float p)
{
	if(p<float(MIN_PARTIALS+1))
		_partials=float(MIN_PARTIALS+1);
	else if(p>float(_max_partials-1))
		_partials=float(_max_partials-1);
	else
		_partials=p;
}

float AudioSynthSinePartial::get_partials(void)
{
	return(_partials);
}

void AudioSynthSinePartial::show_partials(void)
{
	PRINTF("\n===== AudioSynthSinePartial =====\n");
#ifdef USE_CMSIS
	PRINTF("[Using CMSIS] ");
#elif defined(USE_CMSIS_COS)
	PRINTF("[Using CMSIS_COS] ");
#elif defined(USE_FAST_COS)
	PRINTF("[Using FAST_COS] ");
#endif
	PRINTF("Max-Partials: %d, Partial: %3.2f, Resolution: %d\n",get_max_partials(),_partials,get_resolution());

	if(_a && _b)
	{
        	for(uint8_t i=0;i<_max_partials;i++)
                	PRINTF("a[%d] = %5.3f   b[%d] = %5.3f\n", i, _a[i], i, _b[i]);

               	PRINTF("f(x)=%5.5f", _a[0]);
        	for(uint8_t i=1;i<_max_partials;i++)
                	PRINTF("%+5.5f*cos(%f*x*%d)%+5.5f*sin(%f*x*%d)", _a[i], TWO_PI, i, _b[i], TWO_PI, i);
		PRINTF("\n\n");
	}
	else
	{
		PRINTF("No coefficients available.");
	}

#if defined(USE_TEENSY)
	Serial.flush();
#endif
}

void AudioSynthSinePartial::show_partials_compact(const char* label)
{
        show_partials_compact(label, -1);
}

void AudioSynthSinePartial::show_partials_compact(const char* label, int16_t subindex)
{
        if(_a && _b)
        {
                PRINTF("//\n// Precalculated coefficients for %s with %d partials\n//\n",label, _max_partials-1);

                if(subindex >= 0)
                        PRINTF("const float %s_a[%d][USE_PARTIALS+1] = {\n",label, subindex);
                else
                        PRINTF("const float %s_a[USE_PARTIALS+1] = {\n",label);

                for(uint8_t i=0;i<=_max_partials;i++)
                {
                        if(fabs(_a[i]) <= 0.00001)
                                _a[i]=0.0;
                        if(i<_max_partials)
			{
                		PRINTF("#if USE_PARTIALS > %d\n",i);
                                PRINTF("\t%+2.15f, // a[%d]\n",_a[i],i);
				PRINTF("#endif\n");
			}
                        else
                                PRINTF("};\n");
                }

                if(subindex >= 0)
                        PRINTF("const float %s_b[%d][USE_PARTIALS+1] = {\n",label, subindex);
                else
                        PRINTF("const float %s_b[USE_PARTIALS+1] = {\n",label);
                for(uint8_t i=0;i<=_max_partials;i++)
                {
                        if(fabs(_b[i]) <= 0.00001)
                                _b[i]=0.0;

                        if(i<_max_partials)
			{
                		PRINTF("#if USE_PARTIALS > %d\n",i);
                                PRINTF("\t%+2.15f, // b[%d]\n",_b[i],i);
				PRINTF("#endif\n");
			}
                        else
                                PRINTF("};\n");
                }

                PRINTF("\n");
        }
        else
        {
                PRINTF("No coefficients available.");
        }

#if defined(USE_TEENSY)
        Serial.flush();
#endif
}

void AudioSynthSinePartial::bypass(bool b)
{
	_bypass=b;
	if(_bypass == true)
		_phase_accumulator=0.0;
}

uint16_t AudioSynthSinePartial::set_resolution(uint16_t r)
{
	if(r < 2 || r < _max_partials * 2)
		r=_max_partials * 2;

	_resolution=PERIOD/float(r);

	_calculate();

	return(_resolution);
}

uint16_t AudioSynthSinePartial::get_resolution(void)
{
	return((1.0/_resolution*PERIOD)+0.5);
}

void AudioSynthSinePartial::note(uint8_t midinote)
{
	frequency(_midi_note_to_frequency(midinote));
}

float AudioSynthSinePartial::_midi_note_to_frequency(uint8_t note)
{
	const float a = 440.0;
	return (a / 32) * pow(2, ((note - 9) / 12.0));
}

#if defined(USE_TEENSY)
void AudioSynthSinePartial::update(void)
{
	float block_f32[AUDIO_BLOCK_SAMPLES];
	float ph=_phase_accumulator;
	float partial_num_f;
	uint8_t partial_num;
	float partial_remainder;

	audio_block_t *out_block = allocate();
   	if (!out_block)
		return;

	if (_bypass == true || _frequency == 0.0 || _amplitude == 0.0 || !_a  || !_b )
	{
		memset(&out_block->data,0,sizeof(int16_t)*AUDIO_BLOCK_SAMPLES);
		transmit(out_block);
		release(out_block);
		return;
	}

	partial_remainder = modff(_partials , &partial_num_f);
	partial_num = uint8_t(partial_num_f);

	for(uint8_t n=0;n<AUDIO_BLOCK_SAMPLES;n++)
	{
		block_f32[n] = _a[0];
		for(uint8_t p=1; p<partial_num; p++)
			block_f32[n] += _a[p] * _cos(p * ph) + _b[p] * _sin(p * ph);

		if(partial_remainder > 0.0)
                	block_f32[n] += partial_remainder * _a[partial_num] * _cos(partial_num * ph) + partial_remainder * _b[partial_num] * _sin(partial_num * ph);

#if defined(USE_DC_FILTER_FOR_SQUARE_WAVE)
		if(_waveform==WAVE_SQUARE)
			_dc_filter(&block_f32[n]);
#endif

#if !defined(USE_CMSIS)
		if(!_rising)
			out_block->data[n] = float2int(-1.0 *  _amplitude * block_f32[n]);
		else
			out_block->data[n] = float2int(_amplitude * block_f32[n]);
#endif
		ph+=_phase_increment;
	}

#if defined(USE_CMSIS)
	if(!_rising)
		arm_scale_f32(block_f32,(-1.0)*_amplitude,block_f32,AUDIO_BLOCK_SAMPLES);
	else
		arm_scale_f32(block_f32,_amplitude,block_f32,AUDIO_BLOCK_SAMPLES);
	arm_float_to_q15(block_f32,out_block->data,AUDIO_BLOCK_SAMPLES);
#endif

	/*
	for(uint8_t n=0;n<AUDIO_BLOCK_SAMPLES;n++)
	{
		Serial.print(" ");
		Serial.print(out_block->data[n]);
	}
	Serial.println();
	*/

	transmit(out_block);
	release(out_block);

	_phase_accumulator=fmodf(ph,TWO_PI);
}
#endif

void AudioSynthSinePartial::_calculate(void)
{
	if(_a && _b)
	{
		float a=0.0;
		float b=0.0;

		for (float x = -1.0*M_PI; x <= M_PI; x += _resolution)
			a += _f(x) * _resolution;
#if defined(USE_TEENSY)
		__disable_irq();
#endif
		_a[0]=a/TWO_PI;
#if defined(USE_TEENSY)
		__enable_irq();
#endif

		for(uint8_t p=1; p<=_max_partials;p++)
		{
			a=0.0;
			b=0.0;

			for (float x = -1.0*M_PI; x <= M_PI; x += _resolution) {
				a += _f(x) * _cos(p*x) * _resolution;
       				b += _f(x) * _sin(p*x) * _resolution;
    			}
#if defined(USE_TEENSY)
			__disable_irq();
#endif
			_a[p]=a/M_PI;
			_b[p]=b/M_PI;
#if defined(USE_TEENSY)
			__enable_irq();
#endif
		}
	}
	else
	{
		PRINTF("Memory allocation for coefficients was not successful. Stopping here!\n");
#if defined(USE_TEENSY)
		Serial.flush();
#endif
		while(42==42);
	}
}

void AudioSynthSinePartial::_dc_filter(float* x)
{
	float tmp=*x;
	*x = _R * *x + (1 - _R) * _yn;
	_yn=tmp;
}

void AudioSynthSinePartial::set_coefficients(float* a, float* b)
{
#if defined(USE_TEENSY)
        __disable_irq();
#endif
	if(_a)
		delete[] _a;
	_a=a;
		
	if(_b)
		delete[] _b;
	_b=b;
#if defined(USE_TEENSY)
        __enable_irq();
#endif
}

/***********************************************************************
 * Square waveform class
 ***********************************************************************/

AudioSynthSineSquare::AudioSynthSineSquare(uint8_t p) : AudioSynthSinePartial(p) {
	_waveform=WAVE_SQUARE;
	set_duty_cycle(1.0);
}

AudioSynthSineSquare::~AudioSynthSineSquare() {
}

void AudioSynthSineSquare::set_duty_cycle(float dc)
{
	if(dc < DUTY_CYCLE_MIN)
                _duty_cycle=DUTY_CYCLE_MIN;
	else if(dc > 1.0)
                _duty_cycle=DUTY_CYCLE_MAX;
        else
                _duty_cycle=dc;

        _calculate();
}

void AudioSynthSineSquare::frequency(float freq)
{
	AudioSynthSinePartial::frequency(freq);
        _R=1 - (TWO_PI * _frequency / AUDIO_SAMPLE_RATE_EXACT);
}

float AudioSynthSineSquare::get_duty_cycle(void)
{
	return(_duty_cycle);
}

float AudioSynthSineSquare::_f(float x)
{
	float dc=get_duty_cycle();

	if(fmodf(x,M_PI) < M_PI - (dc * M_PI))
		return(-1.0);
	else
		return(1.0);
}

/***********************************************************************
 * Triangle waveform class
 ***********************************************************************/

AudioSynthSineTriangle::AudioSynthSineTriangle(uint8_t p) : AudioSynthSinePartial(p) {
	_waveform=WAVE_TRIANGLE;
}

AudioSynthSineTriangle::~AudioSynthSineTriangle() {
}

float AudioSynthSineTriangle::_f(float x)
{
    float mod = fmod(fabs(x), TWO_PI);
    if (mod <= M_PI) {
        return(2 * mod / M_PI) - 1;
    } else {
        return(-((2 * (mod - M_PI)) / M_PI) + 1);
    }
}

/***********************************************************************
 * Sawtooth waveform class
 ***********************************************************************/

AudioSynthSineSawtooth::AudioSynthSineSawtooth(uint8_t p) : AudioSynthSinePartial(p)
{
	_waveform=WAVE_SAWTOOTH;
	_rising=true;
}

AudioSynthSineSawtooth::AudioSynthSineSawtooth(uint8_t p, bool r) : AudioSynthSinePartial(p)
{
	rising(r);
}

AudioSynthSineSawtooth::~AudioSynthSineSawtooth() {
}

float AudioSynthSineSawtooth::_f(float x)
{
    const float slope = 1.0 / (HALF_PERIOD);

    x = fmod(x, PERIOD);
    if (x < HALF_PERIOD)
        return slope*x;
    else
        return 1.0 - slope*(x - HALF_PERIOD);
}

void AudioSynthSineSawtooth::rising(bool r)
{
	_rising=r;
}

void AudioSynthSineSawtooth::falling(bool r)
{
	rising(!r);
}

/***********************************************************************
 * Wave based waveform class
 ***********************************************************************/

AudioSynthSineWave::AudioSynthSineWave(uint8_t p, const int16_t* wave, const uint32_t len) : AudioSynthSinePartial(p) {
	_waveform=WAVE_WAVEFORM;
	_wave=wave;
	_len=len;
}

AudioSynthSineWave::~AudioSynthSineWave() {
}

float AudioSynthSineWave::_f(float x)
{
	uint32_t pos;

	x=(fmodf(x, HALF_PERIOD)/HALF_PERIOD+1.0)/2.0;
	pos=(x*_len+0.5);

	return(int2float(x*float(_wave[pos])+(1.0-x)*float(_wave[pos+1])));
}

//******************************************************************************************************************
 
/***********************************************************************
 ***********************************************************************
 * AudioSynthSinePartialFast Base class
 ***********************************************************************
 ***********************************************************************/

#if defined(USE_TEENSY)
AudioSynthSinePartialFast::AudioSynthSinePartialFast(uint8_t p=MIN_PARTIALS+1) : AudioStream(0, NULL)
#else
AudioSynthSinePartialFast::AudioSynthSinePartialFast(uint8_t p=MIN_PARTIALS+1)
#endif
{
        _partials=p;
        _max_partials=p+1;
        _frequency=0.0;
        _amplitude=0.0;
	_phase_increment=0.9;
	_phase_accumulator=0.0;
	_rising=false;
	_bypass=true;
        _a=new float[_max_partials]();
        _waveform=-1;
        _yn=0.0;
        _R=1 - (TWO_PI * _frequency / AUDIO_SAMPLE_RATE_EXACT);
}

AudioSynthSinePartialFast::~AudioSynthSinePartialFast()
{
	delete[] _a;
}

void AudioSynthSinePartialFast::begin(void)
{
        if(_partials<MIN_PARTIALS+1)
                _max_partials=MIN_PARTIALS+2;
        else if(_partials>MAX_PARTIALS-1)
                _max_partials=MAX_PARTIALS;

        if(uint8_t(_partials + 0.5) > _max_partials-1)
                _partials=(_max_partials-1);

        _calculate();

        bypass(false);
}

void AudioSynthSinePartialFast::frequency(float freq)
{
        if(freq < 0.0)
                _frequency=0.0;
        if(freq > (AUDIO_SAMPLE_RATE_EXACT/2.0))
                _frequency=(AUDIO_SAMPLE_RATE_EXACT/2.0);
        else
                _frequency=freq;

        _phase_increment=(TWO_PI*_frequency)/AUDIO_SAMPLE_RATE_EXACT;
        _phase_accumulator=0.0;
}

void AudioSynthSinePartialFast::amplitude(float n)
{
        if(_amplitude < 0.0)
	{
                _amplitude=0.0;
		_phase_accumulator=0.0;
	}
        else
                _amplitude=n;
}

uint8_t AudioSynthSinePartialFast::get_max_partials(void)
{
        return(_max_partials-1);
}

void AudioSynthSinePartialFast::set_partials(float p)
{
        if(p<float(MIN_PARTIALS+1))
                _partials=float(MIN_PARTIALS+1);
        else if(p>float(_max_partials-1))
                _partials=float(_max_partials-1);
        else
                _partials=p;
}

void AudioSynthSinePartialFast::show_partials_compact(const char* label)
{
        show_partials_compact(label, -1);
}

void AudioSynthSinePartialFast::show_partials_compact(const char* label, int16_t subindex)
{
        if(_a)
        {
                PRINTF("//\n// Precalculated coefficients for %s with %d partials\n//\n",label, _max_partials-1);

                if(subindex>=0)
                        PRINTF("const float %s_a[%d][USE_PARTIALS+1] = {\n",label,subindex);
                else
                        PRINTF("const float %s_a[USE_PARTIALS+1] = {\n",label);
                for(uint8_t i=0;i<=_max_partials;i++)
                {
                        if(fabs(_a[i]) <= 0.00001)
                                _a[i]=0.0;

                        if(i<_max_partials)
			{
                		PRINTF("#if USE_PARTIALS > %d\n",i);
                                PRINTF("\t%+2.15f, // a[%d]\n",_a[i],i);
                		PRINTF("#endif\n");
			}
                        else
                                PRINTF("};\n");
                }

                PRINTF("\n");
        }
        else
        {
                PRINTF("No coefficients available.");
        }

#if defined(USE_TEENSY)
	Serial.flush();
#endif
}

void AudioSynthSinePartialFast::bypass(bool b)
{
        _bypass=b;
        if(_bypass == true)
                _phase_accumulator=0.0;
}

#if defined(USE_TEENSY)
void AudioSynthSinePartialFast::update(void)
{
	float block_f32[AUDIO_BLOCK_SAMPLES];
	float ph=_phase_accumulator;
        float partial_num_f;
        uint8_t partial_num;
        float partial_remainder;

        audio_block_t *out_block = allocate();
        if (!out_block)
                return;

	if (_bypass == true || _frequency == 0.0 || _amplitude == 0.0 || !_a)
	{
		memset(&out_block->data,0,sizeof(int16_t)*AUDIO_BLOCK_SAMPLES);
                transmit(out_block);
                release(out_block);
                return;
        }

        partial_remainder = modff(_partials , &partial_num_f);
        partial_num = uint8_t(partial_num_f);

	for(uint8_t n=0;n<AUDIO_BLOCK_SAMPLES;n++)
	{
		block_f32[n]=_a[0];
		for(uint8_t p=1; p<_partials; p++)
		{
			if(_a[p]!=0.0)
                        	block_f32[n]+=_a[p]*_internal_update_function(p*ph);

			if(partial_remainder > 0.0)
                        	block_f32[n] += partial_remainder * _a[partial_num] * _internal_update_function(partial_num * ph);
		}

#if defined(USE_DC_FILTER_FOR_SQUARE_WAVE)
                if(_waveform==WAVE_SQUARE)
                        _dc_filter(&block_f32[n]);
#endif

#if !defined(USE_CMSIS)

                if(_rising)
                        out_block->data[n] = float2int(-1.0 *  _amplitude * block_f32[n]);
                else
                        out_block->data[n] = float2int(_amplitude * block_f32[n]);
#endif

		ph+=_phase_increment;
	}

#if defined(USE_CMSIS)
        if(_rising)
                arm_scale_f32(block_f32,(-1.0),block_f32,AUDIO_BLOCK_SAMPLES);
	else
                arm_scale_f32(block_f32,_amplitude,block_f32,AUDIO_BLOCK_SAMPLES);
	arm_float_to_q15(block_f32, out_block->data, AUDIO_BLOCK_SAMPLES);
#endif

	transmit(out_block);
	release(out_block);

	_phase_accumulator=fmodf(ph,2.0*M_PI);
}
#endif

void AudioSynthSinePartialFast::note(uint8_t midinote)
{
	frequency(_midi_note_to_frequency(midinote));
}


float AudioSynthSinePartialFast::_midi_note_to_frequency(uint8_t note)
{
        const float a = 440.0;
        return (a / 32) * pow(2, ((note - 9) / 12.0));
}

float AudioSynthSinePartialFast::_internal_update_function(float phase)
{
#ifdef USE_CMSIS
	return(arm_cos_f32(phase));
#else
	return(cos(phase));
#endif
}

bool AudioSynthSinePartialFast::_internal_use_cos(void)
{
	return(true);
}

void AudioSynthSinePartialFast::_dc_filter(float* x)
{
        float tmp=*x;
        *x = _R * *x + (1 - _R) * _yn;
        _yn=tmp;
}

void AudioSynthSinePartialFast::set_coefficients(float* a)
{
#if defined(USE_TEENSY)
        __disable_irq();
#endif
	if(_a)
		delete[] _a;
	_a=a;
		
#if defined(USE_TEENSY)
        __enable_irq();
#endif
}

/***********************************************************************
 * Square waveform class
 ***********************************************************************/

AudioSynthSineSquareFast::AudioSynthSineSquareFast(uint8_t p) : AudioSynthSinePartialFast(p)
{
	_waveform=WAVE_SQUARE;
        set_duty_cycle(1.0);
}

AudioSynthSineSquareFast::~AudioSynthSineSquareFast() { }

void AudioSynthSineSquareFast::set_duty_cycle(float dc)
{
	if(dc < DUTY_CYCLE_MIN)
                _duty_cycle=DUTY_CYCLE_MIN;
        else if(dc > 1.0)
                _duty_cycle=DUTY_CYCLE_MAX;
        else
                _duty_cycle=dc;

        _calculate();
}

void AudioSynthSineSquareFast::frequency(float freq)
{
        AudioSynthSinePartialFast::frequency(freq);
        _R=1 - (TWO_PI * _frequency / AUDIO_SAMPLE_RATE_EXACT);
}

void AudioSynthSineSquareFast::_calculate(void)
{
	if(_a)
        {
		float a=0.0;

		if(_duty_cycle!=1.0)
			a=(_duty_cycle/2.0)-M_1_PI;
#if defined(USE_TEENSY)
		__disable_irq();
#endif
                _a[0]=a;
#if defined(USE_TEENSY)
                __enable_irq();
#endif


		for(uint8_t p=1; p<_partials;p++)
		{
			a=0.0;

			if(_duty_cycle==1.0)
				if(p%2==0)
					a=0.0;
				else
					a=2*(_amplitude/(p*M_PI))*pow(-1.0,(p-1)/2.0); // special case
			else
				a=2*(_amplitude/(p*M_PI))*_sin(_duty_cycle*p*M_PI_2);
#if defined(USE_TEENSY)
	                __disable_irq();
#endif
                	_a[p]=a;
#if defined(USE_TEENSY)
                	__enable_irq();
#endif
		}
	}
        else
        {
                PRINTF("Memory allocation for coefficients not successful. Stopping here!\n");
#if defined(USE_TEENSY)
                Serial.flush();
#endif
                while(42==42);
        }
}

/***********************************************************************
 * Triangle waveform class
 ***********************************************************************/

AudioSynthSineTriangleFast::AudioSynthSineTriangleFast(uint8_t p) : AudioSynthSinePartialFast(p) {
        _waveform=WAVE_TRIANGLE;
}

AudioSynthSineTriangleFast::~AudioSynthSineTriangleFast() { }

void AudioSynthSineTriangleFast::_calculate(void)
{
	if(_a)
	{
#if defined(USE_TEENSY)
                __disable_irq();
#endif
                _a[0]=0.0;
#if defined(USE_TEENSY)
                __enable_irq();
#endif

		for(uint8_t p=1; p<_partials;p++)
		{
			float a=0.0;

			a=4.0*_amplitude*((1.0-pow(-1.0,p))/(pow(M_PI,2.0)*pow(p,2.0)));

#if defined(USE_TEENSY)
			__disable_irq();
#endif
			_a[p]=a;
#if defined(USE_TEENSY)
			__enable_irq();
#endif
		}
	}
}

/***********************************************************************
 * Sawtooth waveform class
 ***********************************************************************/

AudioSynthSineSawtoothFast::AudioSynthSineSawtoothFast(uint8_t p) : AudioSynthSinePartialFast(p)
{
	_waveform=WAVE_SAWTOOTH;
	_rising=true;
}

AudioSynthSineSawtoothFast::AudioSynthSineSawtoothFast(uint8_t p, bool r) : AudioSynthSinePartialFast(p)
{
	rising(r);
}

AudioSynthSineSawtoothFast::~AudioSynthSineSawtoothFast() { }

void AudioSynthSineSawtoothFast::_calculate(void)
{
	if(_a)
	{
		for(uint8_t p=1; p<=_partials;p++)
		{
			float a=0.0;

			a=(2.0*_amplitude)/(M_PI*p)*pow(-1.0,p)*_rising;

#if defined(USE_TEENSY)
			__disable_irq();
#endif
			_a[p-1]=a;
#if defined(USE_TEENSY)
			__enable_irq();
#endif
		}
#if defined(USE_TEENSY)
		__disable_irq();
#endif
		_a[0]+=M_2_PI;
#if defined(USE_TEENSY)
		__enable_irq();
#endif
	}
}

float AudioSynthSineSawtoothFast::_internal_update_function(float phase)
{
#ifdef USE_CMSIS
	return(arm_sin_f32(phase));
#else
	return(sin(phase));
#endif
}

bool AudioSynthSineSawtoothFast::_internal_use_cos(void)
{
	return(false);
}

void AudioSynthSineSawtoothFast::rising(bool r)
{
	_rising=r;
}

void AudioSynthSineSawtoothFast::falling(bool r)
{
	rising(!r);
}

/***********************************************************************
 * Helpers
 ***********************************************************************/

float _sin(float x)
{
#if defined(USE_FAST_COS)
	return(_fast_sin(x));
#elif defined(USE_CMSIS_COS)
	return(arm_sin_f32(x));
#else
	return(sin(x));
#endif
}

float _cos(float x)
{
#if defined(USE_FAST_COS)
	return(_fast_cos(x));
#elif defined(USE_CMSIS_COS)
	return(arm_cos_f32(x));
#else
	return(cos(x));
#endif
}

#if defined(USE_FAST_COS)
// From: https://github.com/kennyalive/fast-sine-cosine/blob/master/src/main.cpp
#warning Using FAST_COS instead of std::math or CMCSIS
float _fast_sin(float x) {
    const float B = 4.0f / M_PI;
    const float C = -4.0f / (M_PI * M_PI);
    const float P = 0.225f;
    const int s=static_cast<int>(x / M_PI) % 2;

    x=fmodf(x,M_PI);
    float y = B * x + C * x * (x < 0 ? -x : x);

    if(x>0)
            return(copysign(P * (y * (y < 0 ? -y : y) - y) + y,-s));
    else
            return(-1.0 * copysign(P * (y * (y < 0 ? -y : y) - y) + y,s));
}

float _fast_cos(float x) {

    x = (x > 0) ? -x : x;
    x += M_PI_2;

    return _fast_sin(x);
}
#endif
