/*
   AudioSynthSinePartial

   AudioSynthSinePartial(Fast) is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

   The mathematics behind the SyntSinePartialFast class is decribed in
   https://lpsa.swarthmore.edu/Fourier/Series/ExFS.html

MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#pragma once

#if defined(ARDUINO_TEENSY35) || defined(ARDUINO_TEENSY36) || defined(ARDUINO_TEENSY40) || defined(ARDUINO_TEENSY40MM) || defined(ARDUINO_TEENSY41)
#define USE_TEENSY
#endif

#include <cstdio>
#include <limits.h>
#include <cstdint>
#include <cmath>
#if defined(USE_TEENSY)
#include "Arduino.h"
#include "AudioStream.h"
#endif

#define MIN_PARTIALS 1
#define MAX_PARTIALS 31
#define DEFAULT_RESOLUTION 25
#define DUTY_CYCLE_MIN 0.15
#define DUTY_CYCLE_MAX 1.0
#define DAMPING_FACTOR M_SQRT1_2

//#define USE_FAST_COS
//#define USE_CMSIS_COS

#define HALF_PERIOD M_PI
#define PERIOD 2.0*HALF_PERIOD
#if defined(USE_TEENSY) and !defined(USE_FAST_COS)
#define USE_CMSIS
#endif
#define USE_DC_FILTER_FOR_SQUARE_WAVE

#if defined(USE_CMSIS)
#include "arm_math.h"
#endif

#ifndef AUDIO_SAMPLE_RATE_EXACT
#define AUDIO_SAMPLE_RATE_EXACT 44117.64706
#endif
#ifndef TWO_PI
#define TWO_PI 2.0*M_PI
#endif

#if !defined(USE_PARTIALS)
#define USE_PARTIALS 10
#endif
#if USE_PARTIALS < MIN_PARTIALS
#define USE_PARTIALS MIN_PARTIALS
#endif
#if USE_PARTIALS > MAX_PARTIALS
#define USE_PARTIALS MAX_PARTIALS
#endif

#if defined(USE_TEENSY)
#define PRINTF(...) Serial.printf(__VA_ARGS__)
#else
#define PRINTF(...) printf(__VA_ARGS__)
#endif

enum { WAVE_SQUARE, WAVE_TRIANGLE, WAVE_SAWTOOTH, WAVE_WAVEFORM, WAVE_NULL };
const float damping_factor[WAVE_NULL] = { 0.7, 1.0, 1.0 };

#if defined(USE_TEENSY)
class AudioSynthSinePartial : public AudioStream
#else
class AudioSynthSinePartial
#endif
{
public:
	AudioSynthSinePartial(uint8_t p);
	~AudioSynthSinePartial();
	void begin(void);
	void frequency(float freq);
	void amplitude(float n);
	void set_partials(float p);
	float get_partials(void);
	uint8_t get_max_partials(void);
	uint16_t set_resolution(uint16_t r);
	uint16_t get_resolution(void);
	void show_partials(void);
	void show_partials_compact(const char* label);
	void show_partials_compact(const char* label, int16_t subindex);
	void bypass(bool b);
	void note(uint8_t midinote);
	void set_coefficients(float* a, float* b);
#if defined(USE_TEENSY)
	void update(void);
#endif

protected:
	void _calculate(void);
	virtual float _f(float x)=0;
	void _dc_filter(float* x);
	float _midi_note_to_frequency(uint8_t note);

	uint8_t _max_partials;
	float _partials;
	float _frequency;
	float _amplitude;
	float* _a;
	float* _b;
	float _phase_increment;
	float _phase_accumulator;
	bool _bypass;
	float _resolution;
	bool _rising;
	int8_t _waveform;
	float _yn;
	float _R;
};

class AudioSynthSineSquare : public AudioSynthSinePartial
{
public:
	AudioSynthSineSquare(uint8_t p);
	~AudioSynthSineSquare();
	void set_duty_cycle(float dc);
	float get_duty_cycle(void);
	void frequency(float freq);

protected:
        float _f(float x) override;

	float _duty_cycle;
};

class AudioSynthSineTriangle : public AudioSynthSinePartial
{
public:
	AudioSynthSineTriangle(uint8_t p);
	~AudioSynthSineTriangle();

protected:

        float _f(float x) override;
};

class AudioSynthSineSawtooth : public AudioSynthSinePartial
{
public:
	AudioSynthSineSawtooth(uint8_t p);
	AudioSynthSineSawtooth(uint8_t p, bool r);
	~AudioSynthSineSawtooth();
	void rising(bool r);
	void falling(bool r);
protected:

        float _f(float x) override;
};

class AudioSynthSineWave: public AudioSynthSinePartial
{
public:
	AudioSynthSineWave(uint8_t p, const int16_t* wave, const uint32_t len);
	~AudioSynthSineWave();

protected:

        float _f(float x) override;

	const int16_t* _wave;
	uint32_t _len;
};

/*************************************************************************/

#if defined(USE_TEENSY)
class AudioSynthSinePartialFast : public AudioStream
#else
class AudioSynthSinePartialFast
#endif
{
public:
	AudioSynthSinePartialFast(uint8_t p);
	~AudioSynthSinePartialFast();
	void begin(void);
	void frequency(float freq);
	void amplitude(float n);
	uint8_t get_max_partials(void);
	void set_partials(float p);
	float get_partials(void);
	void show_partials(void);
	void show_partials_compact(const char* label);
	void show_partials_compact(const char* label, int16_t subindex);
        void bypass(bool b);
	void note(uint8_t midinote);
	void set_coefficients(float* a);
#if defined(USE_TEENSY)
        void update(void);
#endif

protected:
	virtual void _calculate(void)=0;
	virtual float _internal_update_function(float phase);
	virtual bool _internal_use_cos(void);
	virtual void _dc_filter(float* x);
        float _midi_note_to_frequency(uint8_t note);

	uint8_t _max_partials;
	float _partials;
	bool _bypass;
	bool _rising;
	float _frequency;
	float _amplitude;
	float* _a;
	float _phase_increment;
	float _phase_accumulator;
        int8_t _waveform;
        float _yn;
        float _R;
};

class AudioSynthSineSquareFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineSquareFast(uint8_t p);
	~AudioSynthSineSquareFast();
	void set_duty_cycle(float dc);
	void frequency(float freq);

protected:
        void _calculate(void);

	float _duty_cycle;
};

class AudioSynthSineTriangleFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineTriangleFast(uint8_t p);
	~AudioSynthSineTriangleFast();

protected:
        void _calculate(void);
};

class AudioSynthSineSawtoothFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineSawtoothFast(uint8_t p);
	AudioSynthSineSawtoothFast(uint8_t p, bool r);
	~AudioSynthSineSawtoothFast();
	void rising(bool r);
	void falling(bool r);

protected:
        void _calculate(void);
	float _internal_update_function(float phase);
	bool _internal_use_cos(void);

	float _rising;
};

#ifndef _FLOAT2INT
#define _FLOAT2INT
inline int float2int(float f)
{
	return(f*float(SHRT_MAX)+0.5);
}

inline float int2float(int i)
{
	return(float(i)/float(SHRT_MAX));
}
#endif

#ifndef _MAPFLOAT
#define _MAPFLOAT
inline float mapfloat(float val, float in_min, float in_max, float out_min, float out_max) {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
#endif

float _cos(float x);
float _sin(float x);
#if defined(USE_FAST_COS)
float _fast_cos(float x);
float _fast_sin(float x);
#endif
