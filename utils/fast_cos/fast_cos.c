#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

float fast_sine(float x) {
    const float B = 4.0f / M_PI;
    const float C = -4.0f / (M_PI * M_PI);
    const float P = 0.225f;
    int s=static_cast<int>(x / M_PI) % 2;

    x=fmodf(x,M_PI);
    float y = B * x + C * x * (x < 0 ? -x : x);

    if(x>0)
	    return(copysign(P * (y * (y < 0 ? -y : y) - y) + y,-s));
    else
	    return(-1.0 * copysign(P * (y * (y < 0 ? -y : y) - y) + y,s));
}

float fast_cosine(float x) {

    x = (x > 0) ? -x : x;
    x += M_PI_2;

    return fast_sine(x);
}

int main(int argc, char* argv[]) {
	float f;

	if(argc==2)
		f=atof(argv[1]);
	else
		f=2.0;

	for(float x=-1.0*M_PI; x<=M_PI; x+=0.01)
		printf("%f,%f\n",x,fast_sine(x*f));
}
