#include "synth_sine_partial.h"
#include <cstdio>

#define USE_MAX_PARTIALS 30

AudioSynthSineSquare* square;
AudioSynthSineTriangle* triangle;
AudioSynthSineSawtooth* sawtooth;
AudioSynthSineSquareFast* square_fast;
AudioSynthSineTriangleFast* triangle_fast;
AudioSynthSineSawtoothFast* sawtooth_fast;

int main(void) {
  char label[32];
  uint16_t i=0;

  printf("// Synth_Sine_Partial - precalculated values\n");
  printf("#if !defined(USE_MAX_PARTIALS)\n");
  printf("#define USE_MAX_PARTIALS %d\n", USE_MAX_PARTIALS);
  printf("#endif\n");
  printf("#if !defined(USE_PARTIALS)\n");
  printf("#define USE_PARTIALS %d\n", USE_MAX_PARTIALS);
  printf("#endif\n");
  printf("\n");

  // Square
  square = new AudioSynthSineSquare(USE_MAX_PARTIALS);
  square->amplitude(1.0);
  square->begin();
  for (float dc = 0.1; dc <= 1.0; dc += 0.01) {
    //snprintf(label, sizeof(label), "square_0_%02d", int(dc * 100));
    snprintf(label, sizeof(label), "square");
    square->set_duty_cycle(dc);
    square->show_partials_compact(label,i);
    i++;
  }

  // Triangle
  triangle = new AudioSynthSineTriangle(USE_MAX_PARTIALS);
  triangle->amplitude(1.0);
  triangle->begin();
  snprintf(label, sizeof(label), "triangle");
  triangle->show_partials_compact(label);

  // Sawtooth
  sawtooth = new AudioSynthSineSawtooth(USE_MAX_PARTIALS);
  sawtooth->amplitude(1.0);
  sawtooth->begin();
  snprintf(label, sizeof(label), "sawtooth");
  sawtooth->show_partials_compact(label);

  // Square Fast
  i=0;
  square_fast = new AudioSynthSineSquareFast(USE_MAX_PARTIALS);
  square_fast->amplitude(1.0);
  square_fast->begin();
  for (float dc = 0.1; dc <= 1.0; dc += 0.01) {
    //snprintf(label, sizeof(label), "square_fast_0_%02d", int(dc * 100));
    snprintf(label, sizeof(label), "square_fast");
    square_fast->set_duty_cycle(dc);
    square_fast->show_partials_compact(label,i);
    i++;
  }

  // Triangle Fast
  triangle_fast = new AudioSynthSineTriangleFast(USE_MAX_PARTIALS);
  triangle_fast->amplitude(1.0);
  triangle_fast->begin();
  snprintf(label, sizeof(label), "triangle_fast");
  triangle_fast->show_partials_compact(label);

  // Sawtooth Fast
  sawtooth_fast = new AudioSynthSineSawtoothFast(USE_MAX_PARTIALS);
  sawtooth_fast->amplitude(1.0);
  sawtooth_fast->begin();
  snprintf(label, sizeof(label), "sawtooth_fast");
  sawtooth_fast->show_partials_compact(label);
}
