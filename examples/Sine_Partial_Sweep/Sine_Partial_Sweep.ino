/*
 * For recording audio in Linux via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"

#define TEST_FREQUENCY 10.0
#define USE_MAX_PARTIALS 10

AudioSynthSineSquare sine_waveform_1(USE_MAX_PARTIALS);
//AudioSynthSineTriangle sine_waveform_2(USE_MAX_PARTIALS);
AudioSynthSineSawtooth sine_waveform_2(USE_MAX_PARTIALS);

AudioOutputUSB usb;
//AudioOutputI2S i2s;
AudioOutputPT8211 i2s;

AudioConnection patchCord0(sine_waveform_1, 0, i2s, 0);
AudioConnection patchCord1(sine_waveform_2, 0, i2s, 1);
AudioConnection patchCord2(sine_waveform_1, 0, usb, 0);
AudioConnection patchCord3(sine_waveform_2, 0, usb, 1);


void setup() {
  AudioMemory(16);
  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  sine_waveform_1.frequency(TEST_FREQUENCY);
  sine_waveform_2.frequency(TEST_FREQUENCY);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  sine_waveform_1.begin();
  sine_waveform_2.begin();

  sine_waveform_1.amplitude(1.0);
  sine_waveform_2.amplitude(1.0);

  sine_waveform_1.set_duty_cycle(0.1);

  Serial.printf("<SETUP END>\n");
  delay(1000);
}

void loop() {
  Serial.printf("<LOOP START>\n");

  sine_waveform_1.show_partials();
  sine_waveform_2.show_partials();

  for (float partials = 2.0; partials <= USE_MAX_PARTIALS; partials += 0.1) {
    sine_waveform_1.set_partials(partials);
    sine_waveform_2.set_partials(partials);
    Serial.printf("PARTIALS=%f\n", partials);
    delay(200);
  }

  Serial.printf("<LOOP END>\n");
}
