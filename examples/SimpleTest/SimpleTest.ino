/*
 * For recording audio in Linux via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"

#define TEST_FREQUENCY 440.0
#define USE_PARTIALS 10

//AudioSynthSineSquare sine_waveform(USE_PARTIALS);
//AudioSynthSineTriangle sine_waveform(USE_PARTIALS);
//AudioSynthSineSawtooth sine_waveform(USE_PARTIALS);
AudioSynthSineSawtooth sine_waveform(USE_PARTIALS, true);

AudioSynthWaveformSine sine;
AudioMixer4 mixer;
AudioOutputUSB usb;
AudioOutputI2S i2s;

AudioConnection patchCord0(sine_waveform, 0, mixer, 0);
AudioConnection patchCord2(mixer, 0, usb, 0);
AudioConnection patchCord3(sine, 0, usb, 1);
AudioConnection patchCord4(mixer, 0, i2s, 0);
AudioConnection patchCord5(sine, 0, i2s, 1);

void setup() {
  AudioMemory(32);
  Serial.begin(115200);
  delay(1000);

  Serial.printf("<SETUP START>\n");

  sine_waveform.frequency(TEST_FREQUENCY);
  sine_waveform.amplitude(1.0);
  sine_waveform.show_partials();

  sine.amplitude(1.0);
  sine.frequency(TEST_FREQUENCY);

  mixer.gain(0, 1.0);
  mixer.gain(1, 1.0);
  mixer.gain(2, 1.0);
  mixer.gain(3, 1.0);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  Serial.printf("<SETUP END>\n");
}

void loop() {
  delay(1000);
}
