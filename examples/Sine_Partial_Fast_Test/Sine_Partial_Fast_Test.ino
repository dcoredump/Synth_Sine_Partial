#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"

#define TEST_FREQUENCY 440.0
#define USE_MAX_PARTIALS 10

AudioSynthSineSquareFast sine_waveform_square(3);
AudioSynthSineTriangleFast sine_waveform_triangle(3);
AudioSynthSineSawtoothFast sine_waveform_sawtooth_up(3, true);
AudioSynthSineSawtoothFast sine_waveform_sawtooth_down(3, false);
AudioMixer4 mixer;
AudioOutputUSB usb;
AudioOutputI2S i2s;

AudioConnection patchCord0(sine_waveform_square, 0, mixer, 0);
AudioConnection patchCord1(sine_waveform_triangle, 0, mixer, 1);
AudioConnection patchCord2(sine_waveform_sawtooth_up, 0, mixer, 2);
AudioConnection patchCord3(sine_waveform_sawtooth_down, 0, mixer, 3);
AudioConnection patchCord4(mixer, 0, usb, 0);
AudioConnection patchCord5(mixer, 1, usb, 1);
AudioConnection patchCord6(mixer, 2, i2s, 0);
AudioConnection patchCord7(mixer, 3, i2s, 1);

void setup() {
  AudioMemory(16);
  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  sine_waveform_square.amplitude(1.0);
  sine_waveform_triangle.amplitude(1.0);
  sine_waveform_sawtooth_up.amplitude(1.0);
  sine_waveform_sawtooth_down.amplitude(1.0);

  mixer.gain(0, 1.0);
  mixer.gain(1, 1.0);
  mixer.gain(2, 1.0);
  mixer.gain(3, 1.0);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  Serial.printf("<SETUP END>\n");
  delay(10000);
}

void loop() {
  uint8_t partials, i;

  Serial.printf("<LOOP START>\n");

  for (partials = 3; partials <= USE_MAX_PARTIALS; partials++) {
    Serial.printf("PARTIALS=%d\n", partials);

    for (i = 0; i < 6; i++) {
      note(i, partials, TEST_FREQUENCY);
      delay(500);
      Serial.printf("CPU = %f%%, CPU_MAX = %f%%\n", AudioProcessorUsage(), AudioProcessorUsageMax());
      delay(500);
      note(i, partials, 0.0);
      Serial.printf("--------------------------------------------------------------\n");
      delay(500);
    }
    Serial.printf("==============================================================\n");
  }

  Serial.printf("<LOOP END>\n");

  while (1)
    ;
}

void note(uint8_t w, uint8_t p, float f) {
  if (f != 0.0)
    Serial.printf("Note-On ");
  else
    Serial.printf("Note-Off ");

  switch (w) {
    case 0:
      Serial.printf("SQUARE 20%%\n");
      sine_waveform_square.set_partials(p);
      sine_waveform_square.set_duty_cycle(0.2);
      sine_waveform_square.frequency(f);
      break;
    case 1:
      Serial.printf("SQUARE 80%%\n");
      sine_waveform_square.set_partials(p);
      sine_waveform_square.set_duty_cycle(0.8);
      sine_waveform_square.frequency(f);
      break;
    case 2:
      Serial.printf("SQUARE 50%%\n");
      sine_waveform_square.set_partials(p);
      sine_waveform_square.set_duty_cycle(0.5);
      sine_waveform_square.frequency(f);
      break;
    case 3:
      Serial.printf("TRIANGLE\n");
      sine_waveform_triangle.set_partials(p);
      sine_waveform_triangle.frequency(f);
      break;
    case 4:
      Serial.printf("SAWTOOTH UP\n");
      sine_waveform_sawtooth_up.set_partials(p);
      sine_waveform_sawtooth_up.frequency(f);
      break;
    case 5:
      Serial.printf("SAWTOOTH DOWN\n");
      sine_waveform_sawtooth_down.set_partials(p);
      sine_waveform_sawtooth_down.frequency(f);
      break;
  }
}

