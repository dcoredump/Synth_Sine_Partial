/*
 * For recording audio in Linux via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include "arm_math.h"

#define NUM_TEST_CYCLES 1000000

float _fast_sin(float x) {
  const float B = 4.0f / M_PI;
  const float C = -4.0f / (M_PI * M_PI);
  const float P = 0.225f;
  const int s = static_cast<int>(x / M_PI) % 2;

  x = fmodf(x, M_PI);
  float y = B * x + C * x * (x < 0 ? -x : x);

  if (x > 0)
    return (copysign(P * (y * (y < 0 ? -y : y) - y) + y, -s));
  else
    return (-1.0 * copysign(P * (y * (y < 0 ? -y : y) - y) + y, s));
}

float _fast_cos(float x) {

  x = (x > 0) ? -x : x;
  x += M_PI_2;

  return _fast_sin(x);
}

void setup() {
  float tmp;

  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  Serial.printf("Fast Sine: ");
  elapsedMicros t3;
  for (float i = -M_PI; i < M_PI; i += M_PI / NUM_TEST_CYCLES) {
    tmp = _fast_sin(i);
  }
  Serial.printf("%f\n", float(t3) / NUM_TEST_CYCLES);
  tmp = 0.0;

  Serial.printf("Std Sine: ");
  elapsedMicros t1;
  for (float i = -M_PI; i < M_PI; i += M_PI / NUM_TEST_CYCLES) {
    tmp = sin(i);
  }
  Serial.printf("%f\n", float(t1) / NUM_TEST_CYCLES);
  tmp = 0.0;

  Serial.printf("CMSIS: ");
  elapsedMicros t2;
  for (float i = -M_PI; i < M_PI; i += M_PI / NUM_TEST_CYCLES) {
    tmp = arm_sin_f32(i);
  }
  Serial.printf("%f\n", float(t2) / NUM_TEST_CYCLES);
  tmp = 0.0;

  Serial.printf("%f\n", tmp);

  Serial.printf("<SETUP END>\n");
}

void loop() {
}
