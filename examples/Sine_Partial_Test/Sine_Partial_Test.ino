/*
 * For recording audio in Linux (ALSA) via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"
#include "waveform.h"

#define TEST_FREQUENCY 440.0
#define PARTIALS 3

AudioSynthSineSquare sine_waveform_square(PARTIALS);
AudioSynthSineTriangle sine_waveform_triangle(PARTIALS);
AudioSynthSineSawtooth sine_waveform_sawtooth_up(PARTIALS, true);
AudioSynthSineSawtooth sine_waveform_sawtooth_down(PARTIALS, false);
AudioSynthSineWave sine_waveform_wave(PARTIALS, AKWF_0061_256_DATA, 256);

AudioMixer4 mixer;
AudioMixer4 mixerA;
AudioMixer4 mixerB;
AudioOutputUSB usb;
AudioOutputI2S i2s;

AudioConnection patchCord0(sine_waveform_square, 0, mixerA, 0);
AudioConnection patchCord1(sine_waveform_triangle, 0, mixerA, 1);
AudioConnection patchCord2(sine_waveform_sawtooth_up, 0, mixerA, 2);
AudioConnection patchCord3(sine_waveform_sawtooth_down, 0, mixerA, 3);
AudioConnection patchCord4(sine_waveform_wave, 0, mixerB, 0);

AudioConnection patchCord5(mixerA, 0, mixer, 0);
AudioConnection patchCord6(mixerB, 0, mixer, 1);

AudioConnection patchCord7(mixer, 0, usb, 0);
AudioConnection patchCord8(mixer, 0, usb, 1);
AudioConnection patchCord9(mixer, 0, i2s, 0);
AudioConnection patchCord10(mixer, 0, i2s, 1);

void setup() {
  AudioMemory(16);
  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  sine_waveform_square.begin();
  sine_waveform_triangle.begin();
  sine_waveform_sawtooth_up.begin();
  sine_waveform_sawtooth_down.begin();
  sine_waveform_wave.begin();

  sine_waveform_square.amplitude(1.0);
  sine_waveform_triangle.amplitude(1.0);
  sine_waveform_sawtooth_up.amplitude(1.0);
  sine_waveform_sawtooth_down.amplitude(1.0);
  sine_waveform_wave.amplitude(1.0);

sine_waveform_wave.show_partials();

  mixerA.gain(0, 1.0);
  mixerA.gain(1, 1.0);
  mixerA.gain(2, 1.0);
  mixerA.gain(3, 1.0);
  mixerB.gain(0, 1.0);
  mixerB.gain(1, 1.0);
  mixerB.gain(2, 1.0);
  mixerB.gain(3, 1.0);
  mixer.gain(0, 1.0);
  mixer.gain(1, 1.0);
  mixer.gain(2, 1.0);
  mixer.gain(3, 1.0);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  Serial.printf("<SETUP END>\n");
  delay(2000);
}

void loop() {
  uint8_t partials, i;

  Serial.printf("<LOOP START>\n");
  partials = PARTIALS;
  //for (partials = 3; partials <= USE_MAX_PARTIALS; partials++) {
  Serial.printf("PARTIALS=%d\n", partials);

  for (i = 4; i < 6; i++) {
    note(i, partials, TEST_FREQUENCY);
    delay(500);
    //Serial.printf("CPU = %f%%, CPU_MAX = %f%%\n", AudioProcessorUsage(), AudioProcessorUsageMax());
    delay(1000);
    note(i, partials, 0.0);
    Serial.printf("--------------------------------------------------------------\n");
    delay(500);
  }
  Serial.printf("==============================================================\n");
  //}

  Serial.printf("<LOOP END>\n");

  //while (1)
  // ;
}

void note(uint8_t w, uint8_t p, float f) {
  if (f != 0.0)
    Serial.printf("Note-On ");
  else
    Serial.printf("Note-Off ");

  switch (w) {
    case 0:
      Serial.printf("SQUARE 40%%\n");
      sine_waveform_square.set_partials(p);
      sine_waveform_square.set_duty_cycle(0.4);
      sine_waveform_square.frequency(f);
      break;
    case 1:
      Serial.printf("SQUARE 100%%\n");
      sine_waveform_square.set_partials(p);
      sine_waveform_square.set_duty_cycle(1.0);
      sine_waveform_square.frequency(f);
      break;
    case 2:
      Serial.printf("TRIANGLE\n");
      sine_waveform_triangle.set_partials(p);
      sine_waveform_triangle.frequency(f);
      break;
    case 3:
      Serial.printf("SAWTOOTH UP\n");
      sine_waveform_sawtooth_up.set_partials(p);
      sine_waveform_sawtooth_up.frequency(f);
      break;
    case 4:
      Serial.printf("SAWTOOTH DOWN\n");
      sine_waveform_sawtooth_down.set_partials(p);
      sine_waveform_sawtooth_down.frequency(f);
      break;
    case 5:
      Serial.printf("WAVE\n");
      //sine_waveform_wave.set_partials(p);
      sine_waveform_wave.frequency(f);
      break;
  }
}
