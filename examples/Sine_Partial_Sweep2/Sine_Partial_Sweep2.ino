/*
 * For recording audio in Linux via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"

#define TEST_FREQUENCY 10.0
#define USE_MAX_PARTIALS 20

AudioSynthSineSquare sine_waveform_1(USE_MAX_PARTIALS);

AudioOutputUSB usb;
//AudioOutputI2S i2s;
AudioOutputPT8211 i2s;

AudioConnection patchCord0(sine_waveform_1, 0, i2s, 0);
AudioConnection patchCord1(sine_waveform_1, 0, usb, 0);

void setup() {
  AudioMemory(16);
  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  sine_waveform_1.frequency(TEST_FREQUENCY);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  sine_waveform_1.begin();

  Serial.printf("<SETUP END>\n");
  delay(3000);
}

void loop() {
  Serial.printf("<LOOP START>\n");

  sine_waveform_1.show_partials();

  sine_waveform_1.amplitude(1.0);
  for (float duty_cycle = 0.0; duty_cycle <=1.0; duty_cycle+=0.01) {
    Serial.printf("DUTY_CYCLE=%f\n", duty_cycle);
    sine_waveform_1.set_duty_cycle(duty_cycle);
    delay(100);
  }
  sine_waveform_1.amplitude(0.0);
  delay(1000);

  Serial.printf("<LOOP END>\n");
}
