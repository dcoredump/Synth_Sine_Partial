# Synth_Sine_Partial

AudioSynthSinePartial is a C++ class designed for Teensy Microcontrollers with an FPU
on board (>=3.5). It generates the following waveform types based on addition of sin/cos
waves (iFFT):
- square/pulse
- triangle
- sawtooth (up/down)

This picture shows the waveforms with 3 partials (square 20%, square 50%, triangle, sawtooth):
![Demo waves with 3 partials](doc/pics/Demo_waves_3_partials.jpg)

As you can see, they are very similar to sine waves.

And this picture shows the waveforms with 20 partials (square 20%, square 50%, triangle, sawtooth):
![Demo waves with 20 partials](doc/pics/Demo_waves_20_partials.jpg)

Now the waveforms are well visible. From 5 partials on, relatively good waveforms can be recognized.

An audio file for better examination with an audio editor of free choice is here:

![Audio data (without CMSIS)](doc/Testtones_440Hz_SQ20_SQ50_TRI_SAWUP__3_TO_20_PARTIALS.wav)

![Audio data (with CMCSIS)](doc/Testtones_440Hz_SQ20_SQ50_TRI_SAWUP__3_TO_20_PARTIALS_WITH_CMCSIS.wav)

Each starting with 3 partials up to 20 partials, frequency of the tones is 440Hz, recorded at 44.1kHz.

- square 20%
- square 50%
- triangle
- sawtooth down

![Audio data (50 partials, with CMCSIS)](doc/Testtones_440Hz_SQ20_SQ50_TRI_SAWUP__50_PARTIALS.wav)

[![Demo video on Youtube](https://img.youtube.com/fnFaWrJZqiQ/0.jpg)](https://youtu.be/fnFaWrJZqiQ)

It seems that above a certain number of partials, rounding errors add up and strange artifacts appear. Since the output does not improve after a certain number of partials, it is not advisable to use more than 20 partials per voice.

Here is a rough calculation for the number of partials based on the number of voices and oscillators:

max_partials_per_voice = max_cpu_partials / (number_of_oscillators * number_of_voices)

For max_cpu_partials you can use the numbers from the benchmarks below. Keep in mind that you should keep some headroom for other processes going on (e.g. showing text on display, other effects (reverb), ...). In the end, you probably have to test which value works well.

## Benchmarks

    - Teensy-3.5 (120 Mhz, Fastest)
      - without CMSIS support:      4 partials max
      - with CMCSIS support:        9 partials max
    - Teensy-3.6 (180 MHz, Fastest)
      - without CMSIS support:     11 partials max
      - with CMCSIS support:       23 partials max
    - Teensy-4.0 (600 MHz, Fastest)
      - without CMSIS support:     47 partials max
      - with CMCSIS support:      108 partials max

## Helpful tools:

https://valdivia.staff.jade-hs.de/fft.html

https://www.mathe-fa.de/de

